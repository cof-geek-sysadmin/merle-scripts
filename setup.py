#!/usr/bin/env python3

from setuptools import setup, find_packages


def parse_requirements():
    reqs = []
    with open("requirements.txt", "r") as handle:
        for line in handle:
            reqs.append(line)
    return reqs


setup(
    name="merle-scripts",
    version="1.6.0",
    description="Merle (mattermost at ENS) management tools for sysadmins",
    author="Klub Dev ENS",
    author_email="klub-dev@ens.fr",
    license="LICENSE",
    url="https://git.eleves.ens.fr/cof-geek-sysadmin/merle-scripts/",
    packages=find_packages(),
    include_package_data=True,
    long_description=open("README.md").read(),
    install_requires=parse_requirements(),
    entry_points={
        "console_scripts": [
            (
                "merle-batchimport = "
                "merle_scripts.user_creation.entry_points:batchimport"
            ),
            ("merle-purgeusers = merle_scripts.purge_users.entry_points:purge"),
            (
                "merle-regen-public-id = "
                "merle_scripts.cas_bridge.entry_points:regen_public_ids"
            ),
            (
                "merle-dev-casbridge = "
                "merle_scripts.cas_bridge.entry_points:run_cas_bridge"
            ),
            (
                "merle-dev-pushproxy = "
                "merle_scripts.push_proxy.entry_points:run_push_proxy"
            ),
        ]
    },
)
