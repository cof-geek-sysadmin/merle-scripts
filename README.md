# Custom scripts for Merle

This repository contains all the custom scripts written to make Merle (the
Mattermost installation here at ENS) work properly.

## Installation

There are two ways to install this collection of scripts. In any case, you will
**also** have to install the configuration files.

### Using pip

This is the recommended and easiest method. You can do this from inside a
virtualenv if you wish.

```bash
  # If you want a virtual environment
  virtualenv -p python3 $VENV_NAME
  source $VENV_NAME/bin/activate

  # In any case
  pip install git+https://git.eleves.ens.fr/cof-geek-sysadmin/merle-scripts
```

### By cloning

```bash
  git clone git+https://git.eleves.ens.fr/cof-geek-sysadmin/merle-scripts
  virtualenv -p python3 venv
  source venv/bin/activate
```

### Install the configuartion files

These files should go under `/etc/mattermost/scripts`. If you'd rather put them
somewhere else, you can do so, but you will have to set the environment
variable `CONFIG_DIR` to this directory's path. We assume here that you use
`/etc/` and that you are root.

```bash
mkdir -p /etc/mattermost/scripts
cd /etc/mattermost/scripts
wget https://git.eleves.ens.fr/cof-geek-sysadmin/merle-scripts/blob/master/default_config/user_creation.yml
$EDITOR *.yml  # To edit the appropriate settings
```


## Usage

If you installed the project with `pip` and your virutalenv (if any) is
sourced, the following commands will be available to you from your shell:

* `merle-batchimport`: create the missing users from LDAP
