import logging

from flask import Flask, request, jsonify
from requests import Session, post, HTTPError
from merle_scripts import config
from merle_scripts.common.mattermost_api import MattermostApi
from merle_scripts.common import flask_setup
import mattermostdriver.exceptions


app = Flask(__name__)
config.flask_import(app, "push_proxy")

log = logging.getLogger("push_proxy")
flask_setup.setup_logger(app, loggers=[log])

URL = app.config["PUSH_SERVER"] + app.config["PUSH_URL"]

mattermost_config = config.load("mattermost_server")
MATTERMOST_URL = mattermost_config["scheme"] + "://" + mattermost_config["url"] + ":" + str(mattermost_config["port"])


@app.route(app.config["PUSH_URL"], methods=["POST"])
def send_push_notif():
    req_json = request.get_json(force=True)
    log.debug("Request: {}".format(req_json))

    filtered_json = {
        "ack_id": req_json["ack_id"],
        "platform": req_json["platform"],
        "server_id": req_json["server_id"],
        "device_id": req_json["device_id"],
        "post_id": req_json["post_id"],
        "type": req_json["type"],
        "signature": req_json["signature"],
        "version": "v2",
    }

    if req_json["type"] == "message":
        message = ""
        req_chan = req_json["channel_name"]
        req_msg = req_json["message"]

        # "clear" messages don't have a message and thus aren't to be processed
        message = app.config["STANDARD_MSG"]
        private_msg = False
        if "team_id" not in req_json:
            message = app.config["PRIV_MSG"]
            private_msg = True
        elif app.config["MENTION_INCOMING_MSG"] in req_msg:
            message = app.config["MENTION_MSG"]
        elif app.config["CHAN_NOTIF_INCOMING_MSG"] in req_msg:
            message = app.config["CHAN_NOTIF_MSG"]

        if not private_msg:
            mattermost = MattermostApi()
            try:
                team = mattermost.driver.teams.get_team(req_json["team_id"])
                team_name = team["display_name"]
                message = message.format(team=team_name, chan=req_chan)
            except mattermostdriver.exceptions.ResourceNotFound:
                log.error(
                    "Mattermost: Resource not found: team_id={}".format(
                        req_json["team_id"]
                    )
                )
                log.error(
                    "Context: from {}, request={}".format(
                        request.headers.get("X-Forwarded-For", request.remote_addr),
                        req_json,
                    )
                )
                return ("Error: no such team", 404)

        filtered_json["channel_name"] = "Merle"
        filtered_json["message"] = message
        filtered_json["is_id_loaded"] = True

    if "badge" in req_json:
        filtered_json["badge"] = req_json["badge"]

    log.debug("Filtered: {}".format(filtered_json))

    response = post(URL, json=filtered_json)
    log.debug("Response ({}): {}".format(response.status_code, response.text))
    return (response.text, response.status_code)


@app.route("/api/v1/ack", methods=["POST"])
def ack_received():
    log.error("Got ACK request on /api/v1/ack, id-loaded notifications must be broken")
    return ("Id-loaded notifications are broken", 400)


def mattermost_get(endpoint, http_session):
    response = http_session.get(MATTERMOST_URL + endpoint)
    response.raise_for_status()
    return response.json()


def get_display_name(user, name_format):
    display_name = "@" + user["username"]
    if name_format == "full_name" or name_format == "nickname_full_name":
        full_name = " ".join(
            filter(lambda x: x, [user["first_name"], user["last_name"]])
        )
        if full_name:
            display_name = full_name
    if name_format == "nickname_full_name":
        if user["nickname"]:
            display_name = user["nickname"]
    return display_name

def get_auth_token(request):
	# For some reason (not investigated enough to find the root cause),
	#  the "Authorization" header does not seem to be always set.
	# Let us try the MMAUTHTOKEN cookie as fallback, which should have the same value
	auth_header = request.headers.get("Authorization")
	auth_cookie = request.cookies.get("MMAUTHTOKEN")

	header_prefix = "Bearer "
	if auth_header and auth_header.startswith(header_prefix):
		auth_header = auth_header[len(header_prefix):]

	if auth_header and auth_cookie and auth_header != auth_cookie:
		log.error(f"Authentication header does not match authentication cookie")

	return auth_header if auth_header else auth_cookie

@app.route("/api/v4/notifications/ack", methods=["POST"])
def retrieve_full_notification():
    req_json = request.get_json(force=True)
    log.debug("ACK request: {}".format(req_json))

    post_id = None
    if "post_id" in req_json:
        post_id = req_json["post_id"] # Can be an explicit 'None' in the request json

    if post_id is None:
        # For notifications that aren't posts
        return (jsonify({}), 200)

    http_session = Session()
    http_session.headers.update({"Authorization": "Bearer " + get_auth_token(request)})

    try:
        post_data = mattermost_get("/api/v4/posts/" + post_id, http_session)
        log.debug("post data: {}".format(post_data))
        channel_id = post_data["channel_id"]
        channel_data = mattermost_get("/api/v4/channels/" + channel_id, http_session)
        team_id = channel_data["team_id"]
        if team_id:
            team_data = mattermost_get("/api/v4/teams/" + team_id, http_session)
            channel_name = (
                team_data["display_name"] + " / " + channel_data["display_name"]
            )
        else:
            channel_name = "Merle"
        sender_id = post_data["user_id"]
        sender_user = mattermost_get("/api/v4/users/" + sender_id, http_session)
        try:
            receiver_name_display_pref = mattermost_get(
                "/api/v4/users/me/preferences/display_settings/name/name_format",
                http_session,
            )["value"]
        except (HTTPError, ValueError, KeyError):
            receiver_name_display_pref = "nickname_full_name"
        sender_name = get_display_name(sender_user, receiver_name_display_pref)

        response = {
            "post_id": post_id,
            "message": post_data["message"],
            "team_id": team_id,
            "channel_id": channel_id,
            "channel_name": channel_name,
            "type": post_data["type"],
            "sender_id": sender_id,
            "sender_name": sender_name,
            "version": "v2",
        }

        log.debug("ACK response: {}".format(response))

        return (jsonify(response), 200)
    except HTTPError as e:
        log.warn("ACK error {}: {}".format(e.response.status_code, e.response.text))
        return (e.response.text, e.response.status_code)
