from merle_scripts.push_proxy.mattermost_push_proxy import app
from merle_scripts.command_line import entrypoint


@entrypoint
def run_push_proxy():
    ''' Run a development server for push_proxy '''
    app.run(host='127.0.0.1', port=5050)


if __name__ == '__main__':
    run_push_proxy()

