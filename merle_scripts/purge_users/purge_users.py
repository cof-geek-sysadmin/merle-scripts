import logging

import mattermostdriver.exceptions
from tqdm import tqdm

from merle_scripts.common import clipper_ldap, mattermost_api

logger = logging.getLogger("purge_users")
logger.setLevel(logging.INFO)


def should_deactivate(user, mattermost, ldap):
    """Checks whether this user should be deactivated"""

    # if mattermost.was_ever_active(user["id"]):
    #    return False

    if user["auth_service"] == "gitlab":
        ldap_account = ldap.fetch_username(user["username"])
        if ldap_account is None or "-archived" in ldap_account.homedir:
            return True
    return False


def purge_inactive_users():
    ldap = clipper_ldap.ClipperLDAP()
    mattermost = mattermost_api.MattermostApi()

    to_purge = []
    logger.info("Getting users…")
    all_users = mattermost.get_users()
    logger.info("Evaluating users to purge…")
    for user in tqdm(all_users):
        if should_deactivate(user, mattermost, ldap):
            to_purge.append(user)

    logger.info(
        "Purging %d/%d (%f %%) users -- down to %d.",
        len(to_purge),
        len(all_users),
        len(to_purge) / len(all_users) * 100,
        len(all_users) - len(to_purge),
    )

    for user in tqdm(to_purge):
        try:
            mattermost.set_user_active(user["id"], False)
        except mattermostdriver.exceptions.InvalidOrMissingParameters:
            logger.warning(
                "Could not deactivate user %s (%s) -- exception",
                user["username"],
                user["id"],
            )
            pass
