from merle_scripts.command_line import entrypoint
from . import purge_users


@entrypoint
def purge():
    return purge_users.purge_inactive_users()
