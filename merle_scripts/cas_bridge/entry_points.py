""" mattermost_cas entry point

Use this script as a entry point by calling it.
You can also use this script as a WSGI entry point, using `app`
"""


import argparse
import os
import logging

from merle_scripts.cas_bridge.mattermost_cas import app
from merle_scripts.cas_bridge import models
from merle_scripts.common import mattermost_api
from merle_scripts.command_line import entrypoint

logger = logging.getLogger(__name__)


def parse_args():
    """ Parse command-line arguments """
    parser = argparse.ArgumentParser(description="OAuth <-> CAS bridge")
    parser.add_argument(
        "--local-testing",
        "-g",
        action="store_true",
        help=(
            "Enable local testing mode: use a "
            "locally-generated SSL certificate with testing "
            "ROOT_URL, CLIENT_ID, CLIENT_SECRET and "
            "DEFAULT_SCOPE values, independant from "
            "configuration."
        ),
    )

    return parser.parse_args()


@entrypoint
def run_cas_bridge():
    """ Run a development server for mattermost_cas """
    args = parse_args()
    if args.local_testing:
        os.environ["OAUTHLIB_INSECURE_TRANSPORT"] = "1"
        app.run(host="0.0.0.0")

    else:
        app.run(host="0.0.0.0")


@entrypoint
def regen_public_ids():
    """ Regenerate all public IDs in the database, and sync with Mattermost DB. """
    parser = argparse.ArgumentParser(description="Regenerate public IDs")
    parser.add_argument(
        '--username', '-u', help="Only do this for this username"
    )
    parser.add_argument(
        '--dry-run', '-n', help="Do not actually commit any change",
        action='store_true'
    )
    args = parser.parse_args()

    with app.app_context():
        m_api = mattermost_api.MattermostApi()
        users = []

        if args.username:
            user = models.User.query.filter_by(username=args.username).first()
            if not user:
                print(f"No such user: {args.username}.")
            users.append(user)
        else:
            users = list(models.User.query.all())

        logger.info("Regenerating public IDs…")
        for count, user in enumerate(users):
            logger.info("[%03d/%03d]> %s", count + 1, len(users), user.username)
            user.regen_public_id(mattermost=m_api, dryrun=args.dry_run)
            models.db.session.add(user)

        if not args.dry_run:
            models.db.session.commit()
        else:
            models.db.session.rollback()
        logger.info("Done.")


if __name__ == "__main__":
    run_cas_bridge()
