""" Overlaod a few features of flask-cas """

from functools import wraps

import flask
from flask_cas import login


def login_required(function):
    @wraps(function)
    def wrap(*args, **kwargs):
        if "CAS_USERNAME" not in flask.session:
            flask.session["CAS_AFTER_LOGIN_SESSION_URL"] = (
                flask.request.script_root + flask.request.full_path
            )
            return login()
        else:
            # This part is ugly, but there is no other easy fix, since `flask-cas`
            # doesn't look like an active project accepting PRs anymore. The "clean"
            # solution would be to somehow register a cas username formatter, to be
            # applied from inside `flask_cas.routing.validate`, but there is no way to
            # do this, apart from forking the project.
            if "NORMALIZE_USERNAME" in flask.current_app.config:
                normalizer = flask.current_app.config["NORMALIZE_USERNAME"]
                username = flask.session["CAS_USERNAME"]
                flask.session["CAS_USERNAME"] = normalizer(username)

            return function(*args, **kwargs)

    return wrap
