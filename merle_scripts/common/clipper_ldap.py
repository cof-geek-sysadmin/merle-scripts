""" LDAP information retriever for clipper accounts """

from collections import namedtuple
import ldap
import merle_scripts.config as config

ClipperAccount = namedtuple("ClipperAccount", ["uid", "name", "promo", "homedir"])


class ClipperLDAP:
    def __init__(self):
        """Initialize the LDAP object"""
        ldap_config = config.load("ldap")

        ldap.set_option(ldap.OPT_X_TLS_REQUIRE_CERT, ldap.OPT_X_TLS_NEVER)
        self.ldap_obj = ldap.initialize(
            "ldaps://{}:{}".format(ldap_config["server"], ldap_config["port"])
        )
        self.ldap_obj.set_option(ldap.OPT_REFERRALS, 0)
        self.ldap_obj.set_option(ldap.OPT_PROTOCOL_VERSION, 3)
        self.ldap_obj.set_option(ldap.OPT_X_TLS, ldap.OPT_X_TLS_DEMAND)
        self.ldap_obj.set_option(ldap.OPT_X_TLS_DEMAND, True)
        self.ldap_obj.set_option(
            ldap.OPT_DEBUG_LEVEL, ldap_config.get("debug_level", 255)
        )
        self.ldap_obj.set_option(
            ldap.OPT_NETWORK_TIMEOUT, ldap_config.get("network_timeout", 10)
        )
        self.ldap_obj.set_option(ldap.OPT_TIMEOUT, ldap_config.get("timeout", 10))

    def search(self, filters="(objectClass=*)"):
        """Do a ldap.search_s with the given filters, as specified for
        ldap.search_s"""
        return self.ldap_obj.search_s(
            "dc=spi,dc=ens,dc=fr",
            ldap.SCOPE_SUBTREE,
            filters,
            [
                "uid",
                "cn",
                "homeDirectory",
            ],
        )

    @staticmethod
    def extract_ldap_info(entry, field):
        """Extract the given field from an LDAP entry as an UTF-8 string"""
        return entry[1].get(field, [b""])[0].decode("utf-8")

    @staticmethod
    def get_promotion(home_dir):
        """Deduce a promotion identifier from the user's home directory.
        To do so, we remove the /users/ prefix and the last directory from
        the path and we concatenate the remaining directories in reverse order.
        The result is None if the home directory is not under /users/.

        Standard account home paths look like /users/$year/$department/$login
        therefore the usual promotion identifiers will look like 'litt17'
        or 'maths15'.
        However, some special promotions can be found like 'staffs' or
        'guests'."""

        home_path = home_dir.split("/")
        if home_path[1] != "users":
            return None
        promo_list = home_path[2:-1]  # remove /users/.../login
        promo_list.reverse()  # department before year number fot the main case
        return "".join(promo_list)

    def get_account_list(self):
        """Extract a list of clipper accounts from the LDAP server.
        The result is a list of 3-tuples containing in order the clipper login,
        the name of the user and its promotion (see get_promotion above)."""

        search_res = self.search("(objectClass=*)")

        clipper_list = []

        for entry in search_res:
            uid = self.extract_ldap_info(entry, "uid")
            if len(uid) > 0:
                name = self.extract_ldap_info(entry, "cn")
                homedir = self.extract_ldap_info(entry, "homeDirectory")
                promo = self.get_promotion(homedir)
                clipper_list.append(ClipperAccount(uid, name, promo, homedir))

        return clipper_list

    def fetch_username(self, username):
        """Fetch a ClipperAccount entry for the exact usrname given"""
        search_res = self.search("(uid={})".format(username))
        if not search_res:
            return None

        for entry in search_res:
            uid = self.extract_ldap_info(entry, "uid")
            if uid and uid == username:
                name = self.extract_ldap_info(entry, "cn")
                homedir = self.extract_ldap_info(entry, "homeDirectory")
                promo = self.get_promotion(homedir)
                return ClipperAccount(uid, name, promo, homedir)
        return None
