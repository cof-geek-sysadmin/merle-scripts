""" Interface with the Mattermost API

This file is an interface with the Mattermost API, and exports all the
necessary functions to interact with the Mattermost server """

import random
import string
import hmac
import hashlib
import mattermostdriver
import logging
import requests

import merle_scripts.config as config


class NoSuchUser(Exception):
    """Raised when a user is not found"""

    def __init__(self, description):
        super().__init__()
        self.description = description

    def __str__(self):
        return "No such Mattermost user: {}".format(self.description)


class MattermostApi:
    """Mattermost API connection"""

    def __init__(self, url=None, token=None, port=None, scheme=None, **kwargs):
        """Actually connect to the Mattermost API

        Connect to the Mattermost API, whose endpoint is at `url`, with the
        Personal Authentication Token provided as `token`. When those two
        arguments are left null, they are taken directly from the configuration
        `mattermost_server`.
        All the remaining kwargs will be passed to the
        `mattermostdriver.Driver` object initialized, in case anything else
        must be set for this connection"""

        self.mattermost_config = config.load("mattermost_server")

        if url is None:
            url = self.mattermost_config["url"]
        if token is None:
            token = self.mattermost_config["token"]
        if port is None:
            port = self.mattermost_config["port"]
        if scheme is None:
            scheme = self.mattermost_config["scheme"]

        self.url = url
        self.token = token

        driver_dict = {"scheme": scheme, "url": url, "port": port, "token": token}
        driver_dict.update(kwargs)
        self.driver = mattermostdriver.Driver(driver_dict)
        self.driver.login()

    def _pwgen(self, length):
        alphabet = string.ascii_letters + string.digits
        out = ""
        for _ in range(length):
            out += random.choice(alphabet)
        return out

    def create_clipper_user(
        self,
        clipper,
        promotion=None,  # Incl. department, eg. 'info2018'
        first_name=None,
        last_name=None,
    ):
        """Creates a user on the Mattermost server, using the Gitlab
        authentication method"""
        # TODO error handling

        # Random unguessable password, because Mattermost needs one
        password = self._pwgen(40)

        created_user = self.driver.users.create_user(
            options={
                "username": clipper,
                "email": "{}@clipper.ens.fr".format(clipper),
                "password": password,
                "first_name": first_name,
                "last_name": last_name,
            }
        )
        user_id = created_user["id"]
        self.use_clipper_auth(user_id, clipper)
        if promotion:
            self.update_user_position(user_id, promotion)

        return user_id

    def create_email_user(
        self, username, email, promotion=None, name=None  # For instance 'exté'
    ):
        """Creates a user on the Mattermost server, using the email
        authentication method"""

        password = self._pwgen(40)

        created_user = self.driver.users.create_user(
            options={
                "username": username,
                "email": email,
                "password": password,
                "first_name": "",
                "last_name": name,
            }
        )
        user_id = created_user["id"]
        if promotion:
            self.update_user_position(user_id, promotion)

        return (user_id, password)

    def _derive_auth_data(self, username):
        """Derives the user data (pseudo-gitlab id) from its username

        The auth data *must* be an integer fitting into a Go int64 datatype
        (source: mattermost:model/gitlab/gitlab.go). Assuming a total of about
        2000 users of this server, there is a probability of about 1e-57 of
        collision, which is acceptable. We implement a failsafe mechanism in
        mattermost-cas to lock out a user experiencing a collision."""

        derivation_key = config.load("user_id_derivation")[
            "id_derivation_shared_secret"
        ]
        hex_string = hmac.new(
            derivation_key.encode("utf-8"),
            username.encode("utf-8"),
            digestmod=hashlib.sha256,
        ).hexdigest()
        hex_value = int(hex_string, base=16)
        fitting_threshold = 2**63 - 1
        identifier = hex_value % fitting_threshold

        return str(identifier)

    def use_clipper_auth(self, user_id, username=None):
        """Use Clipper (aka gitlab) authentication method"""
        # TODO error handling

        if username is None:
            username = self.get_username(user_id)

        auth_data = self._derive_auth_data(username)
        self.driver.users.update_user_authentication_method(
            user_id=user_id, options={"auth_service": "gitlab", "auth_data": auth_data}
        )

    def update_user_position(self, user_id, position):
        """Update a user's position (used to store the promotion)"""
        # TODO error handling
        self.driver.users.patch_user(user_id, options={"position": position})

    def get_username(self, user_id):
        """Obtain the username corresponding to a user_id from the API"""
        pass  # TODO

    def user_exists(self, username=None, email=None):
        """Checks whether a user with a given username OR email exists"""
        # TODO error handling
        if username and email:
            return self.user_exists(username=username) or self.user_exists(email=email)
        try:
            logging.disable(logging.ERROR)
            if username:
                self.driver.users.get_user_by_username(username)
            if email:
                self.driver.users.get_user_by_email(email)
            return True
        except mattermostdriver.exceptions.ResourceNotFound:
            return False
        except requests.HTTPError as e:
            data = e.response.json()
            if data["status_code"] == 404:
                return False
            else:
                raise
        finally:
            logging.disable(logging.NOTSET)

    def user_is_gitlab(self, username: str) -> bool:
        """Checks whether a user uses Gitlab auth method"""
        return self.get_user(username)["auth_service"] == "gitlab"

    def get_user(self, username):
        """Get the user data associated to the given username, or raise
        mattermost_api.NoSuchUser"""

        try:
            user = self.driver.users.get_user_by_username(username)
            return user
        except mattermostdriver.exceptions.ResourceNotFound:
            raise NoSuchUser(username)

    def get_user_id(self, username):
        """Get the user_id associated to the given username, or raise
        mattermost_api.NoSuchUser"""

        return self.get_user(username)["id"]

    def get_users(self, page=None, pagesize=100):
        """Get users from Mattermost. Process with chunks of size :pagesize:. If
        :page: is set, returns the corresponding page, or all users if set to None."""

        if page is not None:
            return self.driver.users.get_users(
                params={"per_page": pagesize, "page": page}
            )

        out = []
        cur_page = 0
        while True:
            chunk = self.driver.users.get_users(
                params={"per_page": pagesize, "page": cur_page}
            )
            if chunk:
                out += chunk
                cur_page += 1
            else:
                break
        return out

    def was_ever_active(self, user_id):
        """Returns whether this user was ever active"""
        user_status = self.driver.status.get_user_status(user_id)
        if not user_status or "last_activity_at" not in user_status:
            return False
        return user_status["last_activity_at"] > 0

    def set_user_active(self, user_id, status):
        """Set this user's activity status to :status: (boolean)"""
        self.driver.users.update_user_active_status(user_id, options={"active": status})
