""" Common setup operations for flask """

import logging
import os


def is_gunicorn():
    """ Checks whether we're running gunicorn """
    environ_key = "SERVER_SOFTWARE"

    if environ_key not in os.environ:
        return False
    software = os.environ[environ_key]
    return software.startswith("gunicorn")


def setup_logger(flask_app, loggers=None):
    """ Configures logger for flask """
    if loggers is None:
        loggers = []

    if is_gunicorn():
        gunicorn_logger = logging.getLogger("gunicorn.error")
        for logger in loggers + [flask_app.logger]:
            logger.handlers = gunicorn_logger.handlers
            logger.setLevel(gunicorn_logger.level)
