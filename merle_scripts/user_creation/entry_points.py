from merle_scripts.command_line import entrypoint
from . import batch_import


@entrypoint
def batchimport():
    return batch_import.create_missing_users()
