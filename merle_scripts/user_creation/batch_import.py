import logging

from merle_scripts.common import mattermost_api
from merle_scripts.common import clipper_ldap
import mattermostdriver.exceptions


logger = logging.getLogger("user_creation")
logger.setLevel(logging.INFO)


def create_missing_users():
    ldap = clipper_ldap.ClipperLDAP()
    mattermost = mattermost_api.MattermostApi()

    for user in ldap.get_account_list():
        if mattermost.user_exists(user[0]):
            logger.debug("Skipping existing user %s", str(user))
        else:
            logger.debug("Creating user %s", str(user))
            try:
                mattermost.create_clipper_user(user.uid, user.promo, None, user.name)
            except mattermostdriver.exceptions.InvalidOrMissingParameters as exn:
                if mattermost.user_exists(email="{}@clipper.ens.fr".format(user[0])):
                    logger.error(
                        "User %s has no account but their email address is used. "
                        "Possible renaming?",
                        user[0],
                    )
                    continue
                raise exn from exn

            logger.info("User created: %s", str(user))
