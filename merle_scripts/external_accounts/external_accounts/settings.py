import os
from django.urls import reverse_lazy

from merle_scripts import config

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

conf = config.load("external_accounts")

BASE_PACKAGE = "merle_scripts.external_accounts."

SECRET_KEY = conf["SECRET_KEY"]

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = conf["DEBUG"]

ALLOWED_HOSTS = conf["ALLOWED_HOSTS"]

ADMINS = conf["ADMINS"]

# Application definition

INSTALLED_APPS = [
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django_cas_ng",
    "django.contrib.sites",
    BASE_PACKAGE + "registration",
]

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]

ROOT_URLCONF = BASE_PACKAGE + "external_accounts.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ]
        },
    }
]

WSGI_APPLICATION = BASE_PACKAGE + "external_accounts.wsgi.application"

# Site ID

SITE_ID = 1

# Database
# https://docs.djangoproject.com/en/2.1/ref/settings/#databases

DATABASES = conf["DATABASES"]


# Password validation
# https://docs.djangoproject.com/en/2.1/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator"
    },
    {"NAME": "django.contrib.auth.password_validation.MinimumLengthValidator"},
    {"NAME": "django.contrib.auth.password_validation.CommonPasswordValidator"},
    {"NAME": "django.contrib.auth.password_validation.NumericPasswordValidator"},
]

LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "handlers": {"console": {"class": "logging.StreamHandler"}},
    "loggers": {"merle_scripts": {"handlers": ["console"], "level": "INFO"}},
}


# Internationalization
# https://docs.djangoproject.com/en/2.1/topics/i18n/

LANGUAGE_CODE = "fr-fr"

TIME_ZONE = "Europe/Paris"

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.1/howto/static-files/

STATIC_URL = conf["STATIC_URL"]
STATIC_ROOT = conf["STATIC_ROOT"]


AUTHENTICATION_BACKENDS = (
    "django.contrib.auth.backends.ModelBackend",
    "django_cas_ng.backends.CASBackend",
)

CAS_SERVER_URL = conf["CAS_SERVER"]
CAS_IGNORE_REFERER = True
CAS_REDIRECT_URL = reverse_lazy("request")
CAS_EMAIL_FORMAT = conf["CAS_EMAIL_FORMAT"]
CAS_FORCE_CHANGE_USERNAME_CASE = "lower"
CAS_VERSION = "CAS_2_SAML_1_0"

LOGIN_URL = reverse_lazy("login")
LOGOUT_URL = reverse_lazy("logout")


# Email settings

if DEBUG:
    EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"
