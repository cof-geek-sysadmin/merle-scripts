from django.contrib import admin
from django.urls import path, include

from django_cas_ng import views as django_cas_views

from ..registration import urls


urlpatterns = [
    path("", include(urls)),
    path("login/", django_cas_views.LoginView.as_view(), name="login"),
    path("logout/", django_cas_views.LogoutView.as_view(), name="logout"),
    path("admin/", admin.site.urls),
]
