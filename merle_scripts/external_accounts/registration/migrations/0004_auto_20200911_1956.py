# Generated by Django 2.2.16 on 2020-09-11 17:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('registration', '0003_add_timestamps'),
    ]

    operations = [
        migrations.AlterField(
            model_name='registration',
            name='email',
            field=models.EmailField(max_length=128, verbose_name='Adresse e-mail'),
        ),
    ]
