""" Functions regarding mails sent to the moderators upon some event """

from django.urls import reverse
from django.template.loader import render_to_string
from django.dispatch import receiver
from django.db.models.signals import post_save
from django.contrib.sites.models import Site

from .models import Registration
from .util import moderators_send_mail


def gather_remainder_context(pending=None):
    """ Gather the data used as general context for the validation remainder emails

    `pending` is the query list of pending registrations. Can be passed if it was
    already requested.
    """

    if pending is None:
        pending = Registration.objects.filter(status="0_pnd")

    current_site = Site.objects.get_current()
    site_hostname = current_site.domain
    moderation_url = "https://{}{}".format(site_hostname, reverse("moderation"))

    context = {"count": len(pending), "moderation_url": moderation_url}

    return context


def new_request():
    """ Send a mail to the moderators upon new account request """

    context = gather_remainder_context()
    email_string = render_to_string(
        "registration/email/moderation_new_req.txt", context=context
    )
    return moderators_send_mail(
        "[Merle-extés] Nouvelle demande de compte", email_string
    )


@receiver(
    post_save,
    sender=Registration,
    dispatch_uid="external_accounts.registration.upon_registration_save",
)
def upon_registration_save(sender, instance, created, *args, **kwargs):
    """ Upon creation of a new account request, send a mail to the moderators """
    if created:
        new_request()
