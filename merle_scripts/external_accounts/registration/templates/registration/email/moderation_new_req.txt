Bonjour,

Un nouveau compte extérieur vient d'être demandé sur Merle-extés. Il y a
au total {{ count }} demandes en attente. Vous pouvez consulter et
approuver ces demandes sur {{ moderation_url }}.

Robotiquement,
-- Merle-extés
