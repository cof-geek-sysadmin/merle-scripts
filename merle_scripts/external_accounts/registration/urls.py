from django.urls import path

from .views import RequestView, SuccessView, ModerationView, ModerateView


urlpatterns = [
    path('', RequestView.as_view(), name="request"),
    path('succes/', SuccessView.as_view(), name="success"),
    path('moderation/', ModerationView.as_view(), name="moderation"),
    path('moderation/act/', ModerateView.as_view(), name="moderate"),
]
