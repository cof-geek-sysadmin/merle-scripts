""" Django command to send email digests of pending account requests """

from django.core.management.base import BaseCommand, CommandError
from django.template.loader import render_to_string
from merle_scripts.external_accounts.registration.models import Registration
from merle_scripts.external_accounts.registration.util import moderators_send_mail
from merle_scripts.external_accounts.registration.moderation_mails import (
    gather_remainder_context,
)


class Command(BaseCommand):
    """ Command class handling send_digested_pending_requests """

    help = "Sends a digest of pending registrations to the configured email addresses"

    def handle(self, *args, **options):
        """ Actually do the job """

        pending = Registration.objects.filter(status="0_pnd")

        if not pending.exists():
            # Nothing to do
            self.stdout.write(
                self.style.SUCCESS(
                    "There are no pending registrations — no email was sent"
                )
            )

        else:
            context = gather_remainder_context(pending)
            email_string = render_to_string(
                "registration/email/moderation_digest.txt", context=context
            )

            mails_sent = moderators_send_mail(
                "[Merle-extés] comptes en attente", email_string
            )

            if mails_sent < 1:
                raise CommandError("Sending of mail failed")

            self.stdout.write(
                self.style.SUCCESS(
                    (
                        "Successfully sent email digest to the moderators, "
                        "{} requests pending"
                    ).format(len(pending))
                )
            )
