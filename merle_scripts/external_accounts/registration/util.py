""" Various functions used in various places of this module """

from django.conf import settings
from django.core.mail import send_mail
from django.contrib.auth.models import User


def moderators_send_mail(subject, message):
    """ Send an email to the moderators """

    free_text = ""

    dests = []
    moderators = User.objects.filter(is_staff=True)
    for moderator in moderators:
        if moderator.email:
            dests.append(moderator.email)
        else:
            free_text += "Moderator {} has no email configured.\n".format(
                moderator.username
            )
    if not dests:
        # Use the django admins as a fallback
        dests = list(map(lambda x: x[1], settings.ADMINS))
    if not dests:
        # Use klub-dev
        dests = ["klub-dev@ens.fr"]

    full_text = ""
    if free_text:
        full_text += "NOTES:\n======\n{}\n".format(free_text)
    full_text += message

    return send_mail(subject, full_text, "mattermost@merle.eleves.ens.fr", dests)
