from django.db import models
from django.utils import timezone

statuses = (
    ("0_pnd", "En attente"),
    ("1_suc", "Compte créé"),
    ("2_err", "Erreur"),
    ("3_dcl", "Refusé"),
)


class Registration(models.Model):
    name = models.CharField(
        verbose_name="Prénom et nom", max_length=50, blank=False, null=False
    )
    username = models.CharField(
        verbose_name="Nom d'utilisateur", max_length=32, null=False, blank=False
    )
    email = models.EmailField(
        verbose_name="Adresse e-mail", max_length=128, null=False, blank=False
    )
    reasons = models.CharField(
        verbose_name="Motivations", max_length=255, null=False, blank=False
    )
    status = models.CharField(
        verbose_name="Statut",
        max_length=5,
        null=False,
        blank=False,
        default="0_pnd",
        choices=statuses,
    )
    request_date = models.DateTimeField(
        verbose_name="Date de la demande", null=False, blank=False, default=timezone.now
    )
    answer_date = models.DateTimeField(
        verbose_name="Date de réponse", null=True, blank=True
    )

    def __str__(self):
        return "[%s] %s (%s)" % (self.get_status_display(), self.username, self.name)

    @property
    def status_class(self):
        return self.status[2:]

    class Meta:
        verbose_name = "Demande de compte"
        verbose_name_plural = "Demandes de compte"
        ordering = ["status", "-request_date"]
