from django import forms
from django.conf import settings
from django.core.mail import send_mail
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect
from django.urls import reverse, reverse_lazy
from django.views.generic import ListView, CreateView, TemplateView, RedirectView
from django.utils import timezone
from django.utils.decorators import method_decorator

from merle_scripts.common.mattermost_api import MattermostApi

import re

from .models import Registration
from . import moderation_mails


## Demande de compte


class RegistrationForm(forms.ModelForm):
    class Meta:
        model = Registration
        fields = ["name", "username", "email", "reasons"]
        help_texts = {
            "username": "Choisissez-en un reconnaissable, il sera utilisé pour vous contacter directement. Il sera automatiquement préfixé par un caractère _. Utilisez votre ancien login clipper si vous en aviez un.",
            "email": "Une adresse e-mail pour recevoir vos identifiants et vous connecter.",
            "reasons": "En quelques mots, pourquoi voulez-vous un compte Merle ? Quelles équipes voulez-vous rejoindre ?",
        }

    def clean_username(self):
        username = self.cleaned_data["username"].strip().lower()

        if not re.match(r"^[a-z0-9_.]+$", username):
            raise forms.ValidationError(
                "Le nom d'utilisateur ne peut contenir que des caractères alphanumériques, points ou underscore (_)."
            )

        if Registration.objects.filter(username=username, status="0_pnd").exists():
            raise forms.ValidationError(
                "Un compte avec ce nom d'utilisateur est déjà demandé."
            )

        return username

    def clean_email(self):
        email = self.cleaned_data["email"].strip().lower()

        if Registration.objects.filter(email=email, status="0_pnd").exists():
            raise forms.ValidationError(
                "Un compte avec cette adresse e-mail est déjà demandé."
            )

        return email

    def clean(self):
        cleaned_data = super().clean()

        username = cleaned_data.get("username")
        email = cleaned_data.get("email")

        if username and email:
            api = MattermostApi()
            if api.user_exists("_" + username, email):
                raise forms.ValidationError(
                    "Un compte Merle avec ce nom d'utilisateur ou cette adresse e-mail existe déjà."
                )

        return cleaned_data


class RequestView(CreateView):
    model = Registration
    template_name = "registration/request.html"
    form_class = RegistrationForm
    success_url = reverse_lazy("success")


class SuccessView(TemplateView):
    template_name = "registration/success.html"


## Modération


class StaffRequiredMixin(object):
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_staff:
            messages.error(
                request,
                "Vous n'avez pas les droits pour modérer la création de comptes.",
            )
            return redirect(settings.LOGIN_URL)
        return super().dispatch(request, *args, **kwargs)


class ModerationView(StaffRequiredMixin, ListView):
    model = Registration
    template_name = "registration/list.html"

    def get_queryset(self, *args, **kwargs):
        return Registration.objects.all()


class ModerateView(StaffRequiredMixin, RedirectView):
    http_method_names = ["post"]

    def post(self, request, *args, **kwargs):
        out = super().post(request, *args, **kwargs)

        id = request.POST.get("id")
        try:
            demande = Registration.objects.get(id=id)
        except Registration.DoesNotExist:
            return out

        if demande.status == "0_pnd":
            if "accept" in request.POST:
                api = MattermostApi()
                username = "_" + demande.username

                demande.status = "2_err"
                demande.answer_date = timezone.now()
                demande.save()

                uid, password = api.create_email_user(
                    username, demande.email, "Extérieur", demande.name
                )

                messages.success(request, "Compte %s créé" % username)
                demande.status = "1_suc"
                demande.save()

                send_mail(
                    "[Merle ENS] Compte créé",
                    """
Votre compte Merle a été approuvé !

Vous pouvez désormais vous connecter sur https://merle.eleves.ens.fr/ avec les identifiants suivants :

  Nom d'utilisateur : %s
  Mot de passe temporaire : %s

Merci de changer *immédiatement* votre mot de passe.

Bonne discussion !
L'équipe Merle
                          """
                    % (username, password),
                    "klub-dev@ens.fr",
                    [demande.email],
                )
            else:
                demande.status = "3_dcl"
                demande.save()

        return out

    def get_redirect_url(self):
        return reverse("moderation")
