#!/usr/bin/env python3

import os
import logging
from functools import wraps


def command_line_setup():
    log_level_opt = os.getenv('LOGLEVEL', default='info').lower()
    log_level_map = {
        'debug': logging.DEBUG,
        'info': logging.INFO,
        'warning': logging.WARNING,
        'error': logging.ERROR,
        'critical': logging.CRITICAL,
    }
    invalid_log_level = False
    if log_level_opt in log_level_map:
        log_level = log_level_map[log_level_opt]
    else:
        invalid_log_level = True
        log_level = logging.INFO

    logging.basicConfig(level=log_level)

    if invalid_log_level:
        logging.warning("Invalid LOGLEVEL: {}".format(log_level_opt))


def entrypoint(fct):
    @wraps(fct)
    def wrap(*args, **kwargs):
        command_line_setup()
        return fct(*args, **kwargs)

    return wrap
