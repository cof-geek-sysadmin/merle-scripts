''' Configuration loading script, loads a configuration in YAML format. '''

import yaml
import os


DEFAULT_CONFIG_DIR = '/etc/mattermost/scripts/'


class ConfigurationError(Exception):
    ''' Raised whenever a problem occurs reading or parsing a configuration '''
    pass


cached_conf = {}  # Previously loaded configurations


def load(subproject):
    ''' Load the relevant config for the given subproject '''

    if subproject in cached_conf:
        return cached_conf[subproject]

    filename = '{}.yml'.format(subproject)
    conf_dir = os.environ.get('CONFIG_DIR', DEFAULT_CONFIG_DIR)
    path = os.path.join(conf_dir, filename)

    try:
        with open(path, 'r') as handle:
            out_conf = yaml.load(handle, Loader=yaml.Loader)
            cached_conf[subproject] = out_conf
    except IOError:
        raise ConfigurationError(
            ("Cannot find configuration for {} ({}). Try setting the"
             "environment variable CONFIG_DIR?").format(
                subproject, path))
    except PermissionError:
        raise ConfigurationError(
            "Cannot open configuration for {} ({}): permission denied.".format(
                subproject, path))

    return out_conf


def flask_import(app, subproject):
    ''' Load a config file as a Flask configuration '''

    conf_dict = load(subproject)
    for key in conf_dict:
        app.config[key] = conf_dict[key]
